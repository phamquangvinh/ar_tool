<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Document</title>
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css" integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU" crossorigin="anonymous">

	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css" integrity="sha384-XdYbMnZ/QjLh6iI4ogqCTaIjrFk87ip+ekIjefZch0Y+PvJ8CDYtEs1ipDmPorQ+" crossorigin="anonymous">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Lato:100,300,400,700">

    <!-- Styles -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">
    {{-- <link href="{{ elixir('css/app.css') }}" rel="stylesheet"> --}}

	<link rel="stylesheet" href="../public/access/css/style.css">

	
</head>
<body>
	<div class="container">
		<div class="times">
			<img src="../public/img/icon.png" alt="">
			<span class="times-text"><b>02<sup>st</sup> reminder</b></span>
		</div>
		<div class="content">
			<p>Dear Mr. <br>
				By CC:<br><br>
				Hope that this email finds you well ! <br><br>
				Following our email on date , we still have not received the amount of . If you already settled it, kindly help to send us the bank slip for our record. <br>
				Otherwise, could you please help to advise the payment schedule of the ovedue amount of , with details ara as below. 
			</p>
			<table width="100%" border="1" cellpadding="10">
                            <tr>
                                <th class="heading_table">NAME-AR</th>
                                <th class="heading_table">Inv.Date</th>
                                <th class="heading_table">Inv.No</th>
                                <th class="heading_table">Description</th>
                                <th class="heading_table">Issued by</th>
                                <th class="heading_table">Amount</th>
                                <th class="heading_table">Overdue days</th>
                            </tr>
                             <tr>
                                <th class="total_table">Grand Total</th>
                                <th class="total_table"></th>
                                <th class="total_table"></th>
                                <th class="total_table"></th>
                                <th class="total_table"></th>
                                <th class="total_table">342542345</th>
                                <th class="total_table"></th>
                            </tr>
 
                           
                        </table>
            <p>
            	<br><br>
            	We are highly appreciated if you could revert by date  <br><br>
            	Many thanks. <br>
            	Regards,
            </p>
		</div>
	</div>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.2.3/jquery.min.js" integrity="sha384-I6F5OKECLVtK/BL+8iSLDEHowSAfUo76ZL9+kGAgTRdiByINKJaqTPH/QVNS1VDb" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>
    {{-- <script src="{{ elixir('js/app.js') }}"></script> --}}
</body>
</html>


