@extends('layouts.app')
 
@section('content')
        
        <div class="container ">
          <div class="import">
            
            <div class="col-md-12 lightbox" >
              <table>
                            <tr>
                                <th class="heading_table">NAME-AR</th>
                                <th class="heading_table">Inv.Date</th>
                                <th class="heading_table">Inv.No</th>
                                <th class="heading_table">Description</th>
                                <th class="heading_table">Issued by</th>
                                <th class="heading_table">Amount</th>
                                <th class="heading_table">Overdue days</th>
                            </tr>
                           
                             <tr>
                                <th class="total_table">Grand Total</th>
                                <th class="total_table"></th>
                                <th class="total_table"></th>
                                <th class="total_table"></th>
                                <th class="total_table"></th>
                                <th class="total_table">342542345</th>
                                <th class="total_table"></th>
                            </tr>
 
                           
                        </table>
                        <button class="btn btn-danger btn-md " id="btn_cancel">Cancel</button>
                        <button class="btn btn-success btn-md " id="btn_send">Send</button>
              
            </div>
            <div class="col-md-12 data_master ">
            <div class="panel panel-primary">
 
             <div class="panel-heading">Import and Export Data Into Excel  </div>

 
              <div class="panel-body">    
                  <form action="{!! route($typeImport) !!}" method="post" enctype="multipart/form-data">
                    {{--<input type="hidden" name="_token" value="{{ csrf_token() }}">--}}
                    <div class="row">
                       <div class="col-xs-10 col-sm-10 col-md-10">
                        @if (Session::has('success'))
                           <div class="alert alert-success">{{ Session::get('success') }}</div>
                        @elseif (Session::has('warnning'))
                            <div class="alert alert-warnning">{{ Session::get('warnning') }}</div>
                        @endif
                            <div class="form-group">
                                <label for="" class="col-md-3">Select File to Import:</label>
                                <div class="col-md-9">
                                  <input type="file" name="file" class="form-control">

                                {!! $errors->first('products', '<p class="alert alert-danger">:message</p>') !!}
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-2 col-sm-2 col-md-2 text-center">
                        <button class="btn btn-success">Upload</button>
                        </div>
                    </div>

                    </form>
                  <div class="col-xs-12 col-sm-12 col-md-12">
                      <br/><Br/>

                      <a href="{{ route('product.export',['type'=>'xls']) }}" class="btn btn-primary" style="margin-right: 15px;">Download - Excel xls</a>
                      <a href="{{ route('product.export',['type'=>'xlsx']) }}" class="btn btn-primary" style="margin-right: 15px;">Download - Excel xlsx</a>
                      <a href="{{ route('product.export',['type'=>'csv']) }}" class="btn btn-primary" style="margin-right: 15px;">Download - CSV</a>

                      <br/></div>
            </div>
                   
                   <div >

                        <table width="100%" border="1" cellpadding="10">
                            <tr>
                                @foreach($infor as $infor)
                                <th style="height: 30px">{{$infor}}</th>
                                @endforeach
                            </tr>
 
                            @foreach($data as $item)

                            <tr style="height: 30px">
                                @foreach($value as $temp)
                                <td>{{$item->$temp}}</td>
                                @endforeach
                                <td>    <button class="btn btn-success btn-xs remind" id="btn">Remind</button>    </td>
                            </tr>
                            @endforeach
 
                        </table>
                       </div>
                   </div>
 
             </div>
 
            </div>
            </div>
            </div>
            </div>
@endsection


 