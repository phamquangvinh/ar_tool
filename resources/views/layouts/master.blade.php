<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Document</title>
	<link rel="stylesheet" href="{{asset('css/style.css')}}">
</head>
<body>
	<div>
		@include('layouts.menu')
	</div>
	<div>
		@include('layouts.header')
	</div>
	
	<div>
		<h1>WELCOME</h1>
		@yield('NoiDung')
	</div>
	<div>
		@include('layouts.footer')
	</div>
</body>
</html>