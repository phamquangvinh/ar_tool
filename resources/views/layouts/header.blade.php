<div id="head">
	<div class="modal fade" tabindex="1" role="dialog" aria-labelledby="gridSystemModalLabel" id="myModal">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="gridSystemModalLabel">FACESHOP</h4>
            </div>
            <div class="modal-body">
                <div class="panel with-nav-tabs panel-success">
                    <div class="panel-heading">
                        <ul class="nav nav-tabs">
                            <li><a href="#tab1success" data-toggle="tab" class="active" >ĐĂNG NHẬP</a></li>
                            <li><a href="#tab2success" data-toggle="tab">ĐĂNG KÍ</a></li>
                        </ul>
                    </div>
                    <div class="panel-body">
                        <div class="alert alert-danger alert-dismissable message-model hide">
                          <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                            <div class="content-message-model"></div>
                        </div>
                        <div class="tab-content">
                            <div class="tab-pane fade  in active" id="tab1success">
                                <form>
                                    <div class="input-group">
                                        <span class="input-group-addon" id="basic-addon1"><i class="fa fa-envelope-o"></i></span>
                                        <input type="email" class="form-control" id="login-input-email" name="email" placeholder="Email" aria-describedby="basic-addon1" ng-model="email" ng-change="changeEmail(email)">
                                    </div>
                                    <div class="message login-message-email alert alert-warning hide"></div>
                                    <br>
                                    <div class="input-group">
                                        <span class="input-group-addon" id="basic-addon1"><i class="glyphicon glyphicon-lock"></i></span>
                                        <input type="password" class="form-control" id="login-input-pass" name="password" placeholder="Mật khẩu" aria-describedby="basic-addon1" ng-model="password">
                                    </div>
                                    <div class="message login-message-pass alert alert-warning hide"></div>
                                    <br>
                                    <div class="input-group">
                                        <!-- <span class="input-group-addon" id="captcha"><img id="img_captcha" src="<?=HTP::$resourceUrl?>/captcha.php"/></span> -->
                                        <input type="text" class="form-control" id="login-input-captcha" name="captcha" placeholder="Mã xác nhận" aria-describedby="basic-addon1" ng-model="captcha">
                                    </div>
                                    <div class="message login-message-captcha alert alert-warning hide"></div>
                                    <br>
                                    <div class="text-right">
                                        <span id="error_mesage" style="color: red"></span>
                                        <button type="button" class="btn btn-link forgot" id="test" ng-click="forgot()">Quên mật khẩu</button>
                                        <button type="button" class="btn btn-success btn-login" ng-click="login()">Đăng nhập</button>
                                    </div>
                                </form>
                            </div>
                            <div class="tab-pane fade" id="tab2success">
                                <form action="" method="POST" role="form">
                                    <div class="input-group">
                                        <span class="input-group-addon" id="basic-addon1"><i class="fa fa-user-o"></i></span>
                                        <input type="text" class="form-control" placeholder="Họ Tên" aria-describedby="basic-addon1" id="reg-input-name" name="reg-name" ng-model="regName">
                                    </div>
                                    <div class="message reg-message-name alert alert-warning hide"></div>
                                    <br>
                                    <div class="input-group">
                                        <span class="input-group-addon" id="basic-addon1"><i class="fa fa-envelope-o"></i></span>
                                        <input type="email" class="form-control" id="reg-input-email" name="email" placeholder="Email" aria-describedby="basic-addon1" ng-model="regEmail">
                                    </div>
                                    <div class="message reg-message-email alert alert-warning hide"></div>
                                    <br>
                                    <div class="input-group">
                                        <span class="input-group-addon" id="basic-addon1"><i class="glyphicon glyphicon-lock"></i></span>
                                        <input type="password" class="form-control" id="reg-input-pass" name="password" placeholder="Mật khẩu" aria-describedby="basic-addon1" ng-model="regPass">
                                    </div>
                                    <div class="message reg-message-pass alert alert-warning hide"></div>
                                    <br>
                                    <div class="input-group">
                                        <span class="input-group-addon" id="basic-addon1"><i class="glyphicon glyphicon-phone"></i></span>
                                        <input type="text" class="form-control" id="reg-input-mobile" name="reg-mobile" placeholder="Số điện thoại" aria-describedby="basic-addon1" ng-model="regMobile">
                                    </div>
                                    <div class="message reg-message-mobile alert alert-warning hide"></div>
                                    <br>
                                    <div class="input-group">
                                        <!-- <span class="input-group-addon" id="captcha"><img id="img_captcha" src="<?=HTP::$resourceUrl?>/captcha.php"/></span> -->
                                        <input type="text" class="form-control" id="reg-input-captcha" name="captcha" placeholder="Mã xác nhận" aria-describedby="basic-addon1" ng-model="regCaptcha">
                                    </div>
                                    <div class="message reg-message-capt alert alert-warning hide"></div>
                                    <br>
                                    <div class="text-right">
                                        <button type="submit" class="btn btn-success btn-register" ng-click="register()">Đăng kí</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>