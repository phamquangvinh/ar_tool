<?php

namespace App\Console\Commands;

use App\congno;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Mail;

class CronEmail extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'sendEmail';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $temp = congno::all();
        $today = date('Y-m-d');
        foreach ($temp as $value) {
            if ( $today == $value->ngaytra) {
                Mail::send('mail', ['data' => $value], function ($msg) use ($value) {
                $msg->from('phamquangvinh1702@gmail.com');
                $msg->to($value->emailKH);
                $msg->subject('ITL kinh chao quy khach');
                });
            }
        }
    }



}
