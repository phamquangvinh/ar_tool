<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class user extends Model
{
    protected $table = "users";
    public $timestamps = false;
    protected $fillable = [
        'name', 'id', 'email', 'permission'
    ];
}
