<?php

use App\category;
/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('kieutrang', function(){
	return view('layouts.master');
});
//truyen tham so 
Route::get('quangvinh/{ten}', function($ten){
	echo "<h1> welcome " .$ten ." </h1>";
});
// truyen tham so co dieu kien
Route::get('quangvinh1/{ten}', function($ten){
	echo "<h1> welcome " .$ten ." </h1>";
})->where (['ten' => '[a-zA-Z]+']);

//dinh danh
Route::get('dinhdanh', ['as' => 'myRoute', function(){
	echo "xin chao dinh danh";
}]);
Route::get('dinhdanh', function(){
	echo "day la dinh danh 2";
})->name('myRoute2');

//goi route trong route

Route::get('goiroute', function(){
	return redirect()->route('myRoute2');
});

// goi controller 
Route::get('controller', 'blog@xinChao');

Route::get('controller1/{ten}', 'blog@xinChao');

// URL
Route::get('mypath', 'blog@getURL');

//truyen du lieu tren request
Route::get('getForm', function(){
	return view('postForm');
});

Route::post('postForm', ['as'=>'postForm', 'uses'=>'blog@postForm']);

//cookie

Route::get('setCookie', 'blog@setCookie');
Route::get('getCookie', 'blog@getCookie');

//postfile

Route::get('postFile_view', function(){
	return view('postfile');
});

Route::post('postFile', ['as'=>'postFile', 'uses'=>'blog@postFile']);

//json

Route::get('json', 'blog@getJson');

//view

Route::get('viewlayout/{str}', 'blog@layout');
//query

Route::get('query_db', function(){
	$data = DB::table('order')->get();
	foreach ($data as $row) {
		foreach ($row as $key => $value) {
			echo $key .":" .$value ."<br>";
		}
	}
});

//select
Route::get('query_select', function(){
	$data = DB::table('user')->select('id', 'email', 'name')->get();
	foreach ($data as $row) {
		foreach ($row as $key => $value) {
			echo $key .":" .$value ."<br>";
		}
	}
});
//where
Route::get('query_select_where', function(){
	$data = DB::table('user')->select('id', 'email', 'name')->where('id','>',1)->get();
	foreach ($data as $row) {
		foreach ($row as $key => $value) {
			echo $key .":" .$value ."<br>";
		}
	}
});

//raw
Route::get('query_select_raw', function(){
	$data = DB::table('user')->select(DB::raw('id, email, name as hoten'))->get();
	foreach ($data as $row) {
		foreach ($row as $key => $value) {
			echo $key .":" .$value ."<br>";
		}
	}
});

//model
Route::get('model/save', function(){
	$user = new App\user();
	$user->name = "Trang";
	$user->email = "Trang@mail.com";
	$user->mobile = 149823194;
	$user->address = "dfljksdjfsdf";
	$user->password = "sadfsadf";
	$user -> save();
});
Route::get('model/product', function(){
	$sp = App\product::where('id', '<', '10')->get()->toArray();
	var_dump($sp);
});
//lienket model
Route::get('model/lienket', function(){
	$cate = App\product::find(3)->category->toJson();
	var_dump($cate);
});

//middleware

Route::get('diem', function(){
	echo "ban da co diem";
})->middleware('diem_middleware');
Route::get('loi', function(){
	echo "ban chua co diem";
})->name('loi');

//login
Route::get('formlogin', function(){
	return view('login');
});
Route::post('login', ['as'=>'login', 'uses'=>'MyAuth@login']);

Route::auth();

Route::get('/home', 'HomeController@index');

//session
Route::group(['middleware'=>['web']],function(){
	Route::get('session', function(){
		Session::put('test', 'Laravel');
		echo Session::get('test');
	});
});

//pagination
Route::get('pagination', 'pagination@index');

//test model
Route::get('test', function(){
	$loaisp = category::find(1);
	foreach ($loaisp->sanpham as $value) {
		echo $value->name."<br>";
	}
});

// excel
Route::get('product-list', 'ProductController@form')->name('product.list');
Route::post('product-import', 'ProductController@productsImport')->name('product.import');
Route::get('product-export/{type}', 'ProductController@productsExport')->name('product.export');
Route::get('createTableProduct', function(){
	Schema::create('DetailDebts_L3', function ($table) {
			$table->increments('id');
            $table->string('TaxCode');
            $table->string('InvNo');
            $table->date('InvDate');
            $table->string('Description');
            $table->string('IssuedBy');
            $table->float('Amount');
            $table->float('Overduedays');
            $table->timestamps();
        });
});
Route::get('createTableMasterList', function(){
	Schema::create('MasterList', function ($table) {
            $table->string('TaxCode');
            $table->string('NameAR');
            $table->string('accountReceivable');
            $table->string('Email');
            $table->string('Remind_01');
            $table->string('Remind_02');
            $table->string('Remind_03');
            $table->timestamps();
        });
});
Route::get('createTableLogs', function(){
	Schema::create('logsImport', function ($table) {
            $table->increments('id');
            $table->string('idUser');
            $table->date('timeImport');
            $table->timestamps();
        });
});




Route::get('createTableCongNo', function(){
    Schema::create('congno', function ($table) {
        $table->increments('id');
        $table->string('nameKH');
        $table->string('emailKH');
        $table->date('ngayno');
        $table->date('ngaytra');
        $table->float('sotien');
    });
});
// sent mail
Route::get('sentMail', 'SentEmail@sentEmail');

Route::get('congnoKH-list', 'CongNoKH@form')->name('congno.list');
Route::post('congnoKH-import', 'CongNoKH@congnoImport')->name('congno.import');
Route::get('congnoKH-export/{type}', 'CongNoKH@congnoExport')->name('congno.export');


//ar tool
Route::get('AR-list', 'ARController@form');
Route::post('AR-import', 'ARController@arImport')->name('AP.import');

Route::get('email', function(){
	return view('ar_tool/L3-Eng');
});









