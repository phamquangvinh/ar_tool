<?php

namespace App\Http\Controllers;

use App\congno;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Excel;

class CongNoKH extends Controller
{

    public function form(){
        $congno = congno::get();
        $infor = ['Name', 'Email', 'Ngay No', 'Ngay Tra', 'So Tien'];
        $value = ['nameKH', 'emailKH', 'ngayno', 'ngayno', 'sotien'];
        return view('import')->with(['data'=>$congno, 'typeImport'=>'congno.import', 'infor' => $infor, 'value'=>$value]);
    }


    public function congnoImport(Request $request){
        if($request->hasFile('file')){
            $path = $request->file('file')->getRealPath();
            $data = \Excel::selectSheetsByIndex(0)->load($path)->get();

            if($data->count()){
                foreach ($data as $key => $value) {
                    $congno_list[] = ['nameKH' => $value->name, 'emailKH' => $value->email, 'ngayno' => $value->ngayno,
                        'ngaytra'=>$value->ngaytra,'sotien'=>$value->sotien ];
                }
                if(!empty($congno_list)){
                    congno::insert($congno_list);
                    \Session::flash('success','File improted successfully.');
                }
            }
        }else{
            \Session::flash('warnning','There is no file to import');
        }
        return Redirect::back();
    }


    public function congnoExport($type){
        $congno = congno::select()->get()->toArray();
        return \Excel::create('cognno', function($excel) use ($congno) {
            $excel->sheet('Product Details', function($sheet) use ($congno)
            {
                $sheet->fromArray($congno);
            });
        })->download($type);
    }



}
