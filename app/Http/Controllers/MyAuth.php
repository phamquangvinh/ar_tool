<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\Auth;
use App\user;

class MyAuth extends Controller
{
    function login(Request $Request){
    	$email = $Request['email'];
    	$pass = $Request['password'];
    	if(user::attempt(['email' => $email, 'password' => $pass]))
    		return view('logged');
    	return view('login', ['error'=>'khong thanh cong']);
    }
}
