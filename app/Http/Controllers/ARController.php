<?php

namespace App\Http\Controllers;

use App\AR;
use App\DetailDebts_L1;
use App\DetailDebts_L2;
use App\DetailDebts_L3;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use App\Http\Requests;
use Maatwebsite\Excel\HeadingRowImport;

class ARController extends Controller
{
    public function form(){
        $ar = new AR();
        $ardata = $ar::permission('nv02');
   
        $infor = ['Tax Code', 'Name AR', 'Account receivable', 'Email', 'Remind'];
        $value = ['TaxCode', 'NameAR', 'accountReceivable', 'Email'];
        return view('import')
        ->with(['data'=>$ardata, 'typeImport' => 'AP.import','infor' => $infor, 'value'=>$value ]);
    }

    public function arImport(Request $request){
        if($request->hasFile('file')){
            $path = $request->file('file')->getRealPath();

//master list
            $data_masterlist = \Excel::selectSheets('Master List')->load($path,function($reader){
                $reader->setHeaderRow(4);
                $reader->skip(2);
                $reader->takeColumns(7);
                $reader->all();
            })->get();

            if($data_masterlist->count()){
                $data_ar = AR::get();

                        foreach ($data_ar as $key => $value_ar) {
                            $taxcode[] = $value_ar->TaxCode; 
                        }
                            
                foreach ($data_masterlist as $key => $value) {
                    if($value->tax_code != null && !in_array($value->tax_code, $taxcode)){    
                       $ar_list[] = ['taxcode' => $value->tax_code, 'name_AR' => $value->name_ar, 'accountReceivable' =>$value->account_receivable,
                        'email' => $value->email , 'Remind01' => $value->remind_01, 'Remind02' => $value->remind_02, 'Remind03' => $value->remind_03];
                    }
                }
                if(!empty($ar_list)){
                   
                    AR::insert($ar_list);
                    \Session::flash('success','File improted successfully.');
                }

            }

        
//Detail debst L1
        $data_detaildebts_l1 = \Excel::selectSheets('Details Debts - L1')->load($path,function($reader){
                $reader->setHeaderRow(2);
                $reader->skip(2);
                $reader->takeColumns(8);
                $reader->all();
            })->get();

            if($data_detaildebts_l1->count()){
                foreach ($data_detaildebts_l1 as $key => $value) {
                    if($value->code != null){    
                         $detaildebts_l1_list[] = ['TaxCode' => $value->code, 'InvDate' => $value->inv_date, 'InvNo' =>$value->inv_no,
                        'Description' => $value->description , 'IssuedBy' => $value->issued_by, 'Amount' => $value->amount, 'Overduedays' => $value->overdue_days];
                    }
                }
                
                if(!empty($detaildebts_l1_list)){
                   
                    //DetailDebts_L1::insert($detaildebts_l1_list);
                    \Session::flash('success','File improted successfully.');
                }

            }

//Detail debst L2
        $data_detaildebts_l2 = \Excel::selectSheets('Details Debts - L2')->load($path,function($reader){
                $reader->setHeaderRow(2);
                $reader->skip(2);
                $reader->takeColumns(8);
                $reader->all();
            })->get();

            if($data_detaildebts_l2->count()){
                foreach ($data_detaildebts_l2 as $key => $value) {
                    if($value->code != null){    
                         $detaildebts_l2_list[] = ['TaxCode' => $value->code, 'InvDate' => $value->inv_date, 'InvNo' =>$value->inv_no,
                        'Description' => $value->description , 'IssuedBy' => $value->issued_by, 'Amount' => $value->amount, 'Overduedays' => $value->overdue_days];
                    }
                }
                
                if(!empty($detaildebts_l2_list)){
                   
                    DetailDebts_L2::insert($detaildebts_l2_list);
                    \Session::flash('success','File improted successfully.');
                }

            }

//Detail debst L3
        $data_detaildebts_l3 = \Excel::selectSheets('Details Debts - L2')->load($path,function($reader){
                $reader->setHeaderRow(2);
                $reader->skip(2);
                $reader->takeColumns(8);
                $reader->all();
            })->get();

            if($data_detaildebts_l3->count()){
                foreach ($data_detaildebts_l3 as $key => $value) {
                    if($value->code != null){    
                         $detaildebts_l3_list[] = ['TaxCode' => $value->code, 'InvDate' => $value->inv_date, 'InvNo' =>$value->inv_no,
                        'Description' => $value->description , 'IssuedBy' => $value->issued_by, 'Amount' => $value->amount, 'Overduedays' => $value->overdue_days];
                    }
                }
                
                if(!empty($detaildebts_l3_list)){
                   
                    DetailDebts_L3::insert($detaildebts_l3_list);
                    \Session::flash('success','File improted successfully.');
                }

            }
        }

        else{
            \Session::flash('warnning','There is no file to import');
        }
        return Redirect::back();
    }
}