<?php
 
namespace App\Http\Controllers;
 
use App\congno;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use App\Product;
use Excel;
 
class ProductController extends Controller
{
 
    public function form(){
        $products = product::get();
        $infor = ['Name', 'Description', 'Price'];
        $value = ['name', 'description', 'price'];
        return view('import')->with(['data'=>$products, 'infor' => $infor, 'value' => $value, 'typeImport'=>'product.import']);
    }
 
    public function productsImport(Request $request){
        if($request->hasFile('file')){
            $path = $request->file('file')->getRealPath();
            $data = \Excel::load($path)->get();


            if($data->count()){


                foreach ($data as $key => $value) {


                    $product_list[] = ['name' => $value->name, 'description' => $value->description, 'price' => $value->price];
                }

                if(!empty($product_list)){
                    product::insert($product_list);
                    \Session::flash('success','File improted successfully.');
                }
            }
        }else{
            \Session::flash('warnning','There is no file to import');
        }
        return Redirect::back();
    } 
 
 
    public function productsExport($type){
        $products = Product::select('name','description','price')->get()->toArray();
        return \Excel::create('Products', function($excel) use ($products) {
            $excel->sheet('Product Details', function($sheet) use ($products)
            {
                $sheet->fromArray($products);
            });
        })->download($type);
    }
 
}
 