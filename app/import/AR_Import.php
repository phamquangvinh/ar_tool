<?php
namespace App\Imports;

use Maatwebsite\Excel\Concerns\WithMultipleSheets;

    class UsersImport implements WithMultipleSheets {

        public function sheets()
        {
            return [
            // Select by sheet index
            0 => new FirstSheetImport(),

            // Select by sheet name
            'Other sheet' => new SecondSheetImport
            ];
        }

        /**
         *
         */
        public function getRow()
            {
                $config['import']['startrow'] = 4 ;
            }
    }