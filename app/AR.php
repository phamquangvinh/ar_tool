<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class AR extends Model
{
    protected $table = "masterlist";
    public $timestamps = false;
    protected $fillable = [
        'taxCode', 'nameAR', 'AR', 'email', 'Remind_1','Remind_2', 'Remind_3', 'status'
    ];

    function user(){
        return $this->belongsTo('App\user', 'id', 'idUser');
    }

    public  static function permission($status){
        return $data = DB::table('masterlist')->select()->get();
    }

     public static function insertIgnore(array $attributes = [])
    {
        $model = new static($attributes);

        if ($model->usesTimestamps()) {
            $model->updateTimestamps();
        }

        $attributes = $model->getAttributes();

        $query = $model->newBaseQueryBuilder();
        $processor = $query->getProcessor();
        $grammar = $query->getGrammar();

        $table = $grammar->wrapTable($model->getTable());
        $keyName = $model->getKeyName();
        $columns = $grammar->columnize(array_keys($attributes));
        $values = $grammar->parameterize($attributes);

        $sql = "insert ignore into {$table} ({$columns}) values ({$values})";

        $id = $processor->processInsertGetId($query, $sql, array_values($attributes));

        $model->setAttribute($keyName, $id);

        return $model;
    }
}
