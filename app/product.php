<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class product extends Model
{
    protected $table = "products";
    public $timestamps = false;
    protected $fillable = [
        'name', 'description', 'price',
    ];

    public function category()
    {
    	return $this->belongsTo('App\category','cate_id', 'id');

    }
}
