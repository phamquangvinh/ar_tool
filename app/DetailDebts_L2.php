<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DetailDebts_L2 extends Model
{
     protected $table = "detaildebts_l2";
    public $timestamps = false;
    protected $fillable = [
        'TaxCode', 'InvNo', 'IntDate', 'Description', 'IssuedBy','Amount', 'Overduedays'
    ];
}
