<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class congno extends Model
{
    protected $table = "congno";
    public $timestamps = false;
    protected $fillable = [
        'nameKH', 'emailKH', 'ngayno', 'ngaytra', 'sotien'
    ];

    public function convertDateChange($params)
    {
        $time = '';
        $GMT  = new \DateTimeZone('UTC');

        if($params['dateChange']){//check exist data change and convert date

            //convert time change UTC+7
            $dateChange = \datetime::createfromformat('Y-m-d H:i',$params['dateChange'],new \DateTimeZone('Asia/Ho_Chi_Minh'));

            //convert time change UTC+0
            $time = $dateChange->setTimeZone($GMT)->format('Y-m-d H:i:s');
        }
        else{//if date change empty then set date time current
            $time = $params['dateChangeDefault'];
        }

        return $time;
    }

}
