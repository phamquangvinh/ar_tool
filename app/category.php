<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class category extends Model
{
     protected $table = "category";
    public $timestamps = false;
    function sanpham()
    {
    	return $this->hasMany('App\product', 'cate_id', 'id');
    }
}
