<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DetailDebts_L3 extends Model
{
     protected $table = "detaildebts_l3";
    public $timestamps = false;
    protected $fillable = [
        'TaxCode', 'InvNo', 'IntDate', 'Description', 'IssuedBy','Amount', 'Overduedays'
    ];
}
