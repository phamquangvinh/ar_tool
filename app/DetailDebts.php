<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DetailDebts extends Model
{
    protected $table = "detaildebts_l1";
    public $timestamps = false;
    protected $fillable = [
        'TaxCode', 'InvNo', 'IntDate', 'Description', 'IssuedBy','Amount', 'Overduedays'
    ];
}
